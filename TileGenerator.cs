﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileGenerator : MonoBehaviour {

    public GameObject tilePrefab;
    public int dimentions;
    public Canvas canvas;
    public GridLayoutGroup gridLayout;   
    public Tile[,] grid;

	// Use this for initialization
	public void GenerateTiles (TattleShipManager manager)
    {
        grid = new Tile[dimentions, dimentions];
        Rect rect = gridLayout.GetComponent<RectTransform>().rect;
        gridLayout.cellSize = new Vector2(rect.width/dimentions, rect.height / dimentions);

        for(int ii=0; ii<dimentions; ii++)
        {
            for (int jj=0; jj<dimentions; jj++)
            {
                GameObject temp = Instantiate<GameObject>(tilePrefab);
                temp.GetComponent<Button>().onClick.AddListener(() => manager.TileTapped(temp));
                temp.transform.SetParent(gridLayout.transform);
                Tile tile = temp.GetComponent<Tile>();
                tile.position = new Vector2(ii, jj);
                grid[jj, ii] = tile;                
            }
        }
	}
}
