﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface TileObserver
{
    void TileTapped(Tile tile);        
}

public class Tile : MonoBehaviour
{
    public bool tapped;
    public Vector2 position;

    private bool glowing = false;
    private float timeWrapped;
    private float time;
    private Image image;
    private Color originalColor;
    private Color fromColor;
    private Color toColor;
    private float lerpsPerSecond;

    public void Glow(Color toColor, float lerpsPerSecond)
    {
        if (!glowing) //check if is already glowing
        {
            image = GetComponent<Image>();
            fromColor = image.color;
            this.toColor = toColor;
            this.lerpsPerSecond = lerpsPerSecond;
            time = 0;
            timeWrapped = 0;
            glowing = true;

            StartCoroutine("GlowAnimation");
        }
    }

    public void CancelGlow()
    {
        glowing = false;
    }

    IEnumerator GlowAnimation()
    {
        while (glowing)
        {
            image.color = Color.Lerp(fromColor, toColor, timeWrapped);
            time += Time.deltaTime;
            timeWrapped += Time.deltaTime * lerpsPerSecond;
            if (timeWrapped > 1)
            {
                timeWrapped--;
            }

            yield return new WaitForEndOfFrame();
        }

        image.color = fromColor;

        yield return null;
    }
}
