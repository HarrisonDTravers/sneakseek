﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coin : MonoBehaviour
{
    public float maxSpeed = 12.0f;
    public int additionals = 30;
    private float time;
    private int side = 0;
    private TattleShipManager manager;

    public GameObject winnerGO;
    public GameObject loserGO;

    public void Flip(TattleShipManager manager, Player winner, Player loser)
    {
        this.manager = manager;
        winnerGO.GetComponentInChildren<Text>().text = winner.shortName;
        winnerGO.GetComponent<Image>().color = winner.color;
        loserGO.GetComponentInChildren<Text>().text = loser.shortName;
        loserGO.GetComponent<Image>().color = loser.color;

        StartCoroutine("FlipAnimation");
    }

    private float rotation;
    IEnumerator FlipAnimation()
    {
        
        while (time < 5)
        {
            side = ((int)(rotation-90)%360)/180;
            if (time >= 0 && time < 3)
            {
                rotation = 180 * maxSpeed * time + additionals * 360.0f;
            }
            else if (time>=3 && time <5)
            {
                rotation = (((-(90 * maxSpeed) * Mathf.Pow(time, 2)) / 2) + ((450*maxSpeed) * time));
            }
            if (side == 1)
            {
                winnerGO.SetActive(false); //hide winner size, show loser side
                loserGO.SetActive(true);
                loserGO.transform.rotation = Quaternion.Euler(0, rotation, 0);
            }
            else
            {
                winnerGO.SetActive(true); //hide loser side show winner side
                loserGO.SetActive(false);
                winnerGO.transform.rotation = Quaternion.Euler(0, 180+rotation, 0);
            }

            time += Time.deltaTime; //this increases time          

            yield return new WaitForEndOfFrame(); //you can ignore this
        }

        manager.dialog.NextDialog();
        yield return null; //you can ignore this
    }
    
}
