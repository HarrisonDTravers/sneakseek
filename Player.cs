﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Players
{
    public static Player GetPlayer1(int coord, bool isRow)
    {
        Player player1 = new Player();
        player1.id = 0;
        player1.name = "Player 1";
        player1.shortName = "P1";
        player1.color = Color.green;
        player1.coordinate = coord;
        player1.isRow = isRow;
        return player1;
    }

    public static Player GetPlayer2(int coord, bool isRow)
    {
        Player player2 = new Player();
        player2.id = 0;
        player2.name = "Player 2";
        player2.shortName = "P2";
        player2.color = Color.cyan;
        player2.coordinate = coord;
        player2.isRow = isRow;
        return player2;
    }
}

public class Player
{
    public string name;
    public int id;
    public int coordinate;
    public bool isRow;
    public string shortName;
    public Color color;
}
