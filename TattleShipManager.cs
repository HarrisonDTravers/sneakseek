﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TattleShipManager : MonoBehaviour
{
    
    public TileGenerator tileGenerator;
    public Dialog dialog;
    public Vector2Int Target { get; set; }
    
    public Color correctTileColor = Color.yellow;
    public Coin coin;
    public AudioClip winSound;
    public AudioClip startTheme;
    public AudioClip coinSpinSound;
    public AudioClip gameTheme;
    public AudioSource audioSource;
    public Player Player1 { get; set; }
    public Player Player2 { get; set; }
    public Player GoesFirst { get; set; } //who goes first
    public GameObject gridRaycastBlocker;
    public GameObject restartPanel;
    public VerticalLayoutGroup pastMovesList;
    public Text pastMoveEntryPrefab;

    private Player playersTurn;
    public Player PlayersTurn
    {
        get { return playersTurn; }
        set
        {
            dialog.text_WhosTurnItIs.color = value.color;
            dialog.text_WhosTurnItIs.text = value.name;
            playersTurn = value;
        }
    }   //whos turn it is

    private bool canTap;
    public bool CanTap
    {
        get { return canTap; }
        set
        {
            gridRaycastBlocker.SetActive(!value);
            canTap = value;
        }
    }

    // Use this for initialization

    void Start ()
    {
        glowingTiles = new List<Tile>();
        Target = new Vector2Int(Random.Range(0, tileGenerator.dimentions), Random.Range(0, tileGenerator.dimentions));
        tileGenerator.GenerateTiles(this);
                
        if (Random.Range(0, 2) == 0)//determine if player 1 is rows or cols
        {
            Player1 = Players.GetPlayer1(Target.x + 1, false); //plus one so that the first tile is tile 1 and not 0
            Player2 = Players.GetPlayer2(Target.y + 1, true);
        }
        else
        {
            Player1 = Players.GetPlayer1(Target.y + 1, true);
            Player2 = Players.GetPlayer2(Target.x + 1, false);
        }

        if (Random.Range(0, 2) == 0)
            GoesFirst = Player1;
        else
            GoesFirst = Player2;

        PlayersTurn = GoesFirst;
        dialog.StartDialog(this);
    }

    public void Restart()
    {
        print("RESTART");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void DialogComplete()
    {
        audioSource.clip = gameTheme;
        audioSource.loop = true;
        audioSource.Play();
        CanTap = true;
    }

    private List<Tile> glowingTiles;
    public void GlowRow(int rowId)
    {
        for (int ii = 0; ii < tileGenerator.dimentions; ii++)
        {
            Tile targetTile = tileGenerator.grid[rowId, ii];
            targetTile.Glow(correctTileColor, 2);
            glowingTiles.Add(targetTile);
        }
    }

    public void GlowCol(int colId)
    {
        for (int ii = 0; ii < tileGenerator.dimentions; ii++)
        {
            Tile targetTile = tileGenerator.grid[ii, colId];
            targetTile.Glow(correctTileColor, 2);
            glowingTiles.Add(targetTile);
        }
    }

    public void CancelGlow()
    {
        foreach (Tile glowingTile in glowingTiles)
            glowingTile.CancelGlow();

        glowingTiles.Clear();
    }

    public void TileTapped(GameObject inTile)
    {
        Tile tile = inTile.GetComponent<Tile>();
        print(tile.position + " : " + Target);
        if (CanTap && !tile.tapped)
        {
            tile.GetComponent<Image>().color = PlayersTurn.color;
            Text instance = Instantiate<Text>(pastMoveEntryPrefab);
            print(ColorUtility.ToHtmlStringRGBA(playersTurn.color));
            instance.text = "<color=#" + ColorUtility.ToHtmlStringRGBA(playersTurn.color) + ">" + playersTurn.name + "</color>:Row " + (tile.position.x+1) + ",Col " + (tile.position.y+1);
            instance.gameObject.transform.SetParent(pastMovesList.transform);
            instance.gameObject.transform.SetAsFirstSibling();
            tile.tapped = true;

            if (tile.position == Target)
            {
                GlowRow(Target.y);
                GlowCol(Target.x);
                audioSource.clip = winSound;
                dialog.text_Turn.text = "Winner";
                dialog.text_Turn.color = correctTileColor;
                audioSource.Play();
                audioSource.loop = false;
                gridRaycastBlocker.GetComponent<Image>().color = new Color(correctTileColor.r, correctTileColor.g, correctTileColor.b, 0.2f);
                CanTap = false;
                restartPanel.SetActive(true);
                StartCoroutine("SpinGameObject", restartPanel.GetComponentInChildren<Image>().gameObject);
            }           
            else //if the game hasn't yet been won, then change turns
            {
                if (PlayersTurn == Player1)
                    PlayersTurn = Player2; //If it was player 1s turn then make it player 2
                else
                    PlayersTurn = Player1; //Vice versa
            }
        }
    }

    IEnumerator SpinGameObject(GameObject go)
    {
        while(true)
        {
            go.transform.Rotate(0, 0, Time.deltaTime*100);
            yield return new WaitForEndOfFrame();
        }
    }
}
