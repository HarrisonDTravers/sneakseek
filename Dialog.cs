﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialog : MonoBehaviour {

    private int ii;

    public GameObject dialogPanel;

    public GameObject welcomePage;

    public GameObject playerPage;
    public Text text_playerName;
    public Text text_playerInstructions;
    public Text text_playerCoordinate;
    public Text text_playerTapToX;

    public GameObject firstTurnPage;
    public Text text_GoesFirst;

    public GameObject GameInfo;
    public Text text_Turn;
    public Text text_WhosTurnItIs;    

    string player1CoordStr;
    GlowCartesian player1Glow;
    string player2CoordStr;
    GlowCartesian player2Glow;

    public bool CanContinue { get; set; }
    private TattleShipManager manager;
    private bool started;

    public delegate void GlowCartesian(int coord);

    public void StartDialog(TattleShipManager manager)
    {
        CanContinue = true;
        this.manager = manager;
        started = true;

        if (manager.Player1.isRow)
        {
            player1CoordStr = "Column:" + manager.Player1.coordinate;
            player1Glow = manager.GlowRow;
            player2CoordStr = "Row:" + manager.Player2.coordinate;
            player2Glow = manager.GlowCol;
        }
        else
        {
            player1CoordStr = "Row:" + manager.Player1.coordinate;
            player1Glow = manager.GlowCol;
            player2CoordStr = "Column:" + manager.Player2.coordinate;
            player2Glow = manager.GlowRow;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(started && CanContinue && Input.GetMouseButtonDown(0))
        {
            NextDialog();
        }
	}

    public void NextDialog()
    {
        switch(ii)
        {
            /*Player1.coordinate = "Row " + (manager.Target.x + 1).ToString();
            Player1.coordinate = "Column " + (manager.Target.y + 1).ToString*/
            case 0:
                welcomePage.SetActive(false);
                playerPage.SetActive(true);
                text_playerName.text = manager.Player1.name;
                text_playerTapToX.text = "Tap to show your co-ordinate.";
                break;
            case 1:
                text_playerCoordinate.text = player1CoordStr;
                text_playerTapToX.text = "Tap when you've remembered your co-ordinate.";
                player1Glow(manager.Player1.coordinate-1);
                break;
            case 2:
                text_playerCoordinate.text = "";
                text_playerName.text = manager.Player2.name;
                text_playerInstructions.text = "Now it's player 2's turn to look at their co-ordinate, make sure player 1 is looking away.";
                text_playerTapToX.text = "Tap to show your co-ordinate.";
                manager.CancelGlow();
                break;
            case 3:
                text_playerCoordinate.text = player2CoordStr;
                text_playerTapToX.text = "Tap when you've remembered your co-ordinate.";
                player2Glow(manager.Player2.coordinate-1);
                break;
            case 4:
                playerPage.SetActive(false);
                firstTurnPage.SetActive(true);
                if(manager.GoesFirst == manager.Player1)                
                    manager.coin.Flip(manager, manager.Player1, manager.Player2);
                else
                    manager.coin.Flip(manager, manager.Player2, manager.Player1);
                manager.audioSource.clip = manager.coinSpinSound;
                manager.audioSource.Play();
                manager.audioSource.loop = false;
                CanContinue = false;
                manager.CancelGlow();
                break;
            case 5:
                text_GoesFirst.color = manager.GoesFirst.color;
                text_GoesFirst.text = (manager.GoesFirst.name) + " is first!";
                CanContinue = true;
                break;
            case 6:
                dialogPanel.gameObject.SetActive(false);
                GameInfo.gameObject.SetActive(true);
                manager.DialogComplete();
                break;
        }
        ii++;
    }
}
